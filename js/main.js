let enterBtn = document.querySelector('.enterBtn');
let confirmBtn = document.querySelector('.enterInBtn');
let createBtn = document.querySelector('.createBtn');
let email = document.querySelector('.emailInput');
let password = document.querySelector('.passInput');
let text = document.querySelector('.currentText');

const userEmail = 'hidoctor@gmail.com';
const userPassword = '123456';
const token = 'ca600cc2-fed9-4991-9b97-b7eb3e098396';

// Правила валидации.
// 1. Инициалы начинаются с большой буквы, состоят из 3 слов, каждое с большой буквы
// 2. Описание - текст. Просто что бы не был пустой.
// 3. Цель - тоже текст. Что бы не было пустым.
// 4. Давление. 3 цифры / 2 цифры.
// 5. ИМТ. Состоит из 2 цифр.
// 6. Болезни. Обязательное для ввода текста, может быть любая инфа.
// 7. Возраст. От 1 года до 100 лет. 
// 8. При неправильном вводе данных должно появляться сообщение 
// о вводе некорректных данных


// Vlad part 

   function process(){
        const em = document.querySelector('.emailInput').value;
        const ps = document.querySelector('.passInput').value;

        if (em == userEmail && ps == userPassword){

            fetch("https://ajax.test-danit.com/api/v2/cards/login", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ email: `${userEmail}`, password: `${userPassword}` })
                })
                .then(response => response.text())
                .then(token => console.log(token));

                let createBtnElement = document.createElement('button');
                createBtnElement.classList.add('createBtn');
                createBtnElement.innerHTML = 'Create visit';
                createBtnElement.setAttribute('data-target', '#createModal');
                createBtnElement.setAttribute('data-toggle', 'modal');
                enterBtn.replaceWith(createBtnElement);
            }
            else { throw new Error("Incorrect password or email"); }
    }



class Visit {
    constructor(purpose, descr, urg, init){
        this.purpose = purpose;
        this.description = descr;
        this.urgency = urg;
        this.initials = init;
    }
    
}

class VisitTherapist extends Visit {
    constructor(age, ...args){
        super(...args);
        this.age = age;
    }
    async createCard(){
        let res = await fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
        },
    body: JSON.stringify({
        title: 'Визит к терапевту',
        description: this.description,
        doctor: 'Терапевт',
        purpose: this.purpose,
        urgency: this.urgency,
        initials: this.initials,
        age: this.age
        })
    })

    let info = await res.json();
    this.id = await info.id;
    return await info.id;

}


}

class VisitCardiologist extends Visit {
    constructor(pres, bmi, dis, age, ...args){
        super(...args);
        this.pressure = pres;
        this.bmi = bmi;
        this.diseases = dis;
        this.age = age;
    }
    async createCard(){
        let res = await fetch("https://ajax.test-danit.com/api/v2/cards", {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
            },
        body: JSON.stringify({
            title: 'Візит до кардіолога',
            description: this.description,
            doctor: 'Кардіолог',
            purpose: this.purpose,
            urgency: this.urgency,
            initials: this.initials,
            pressure: this.pressure,
            bmi: this.bmi,
            diseases: this.diseases,
            age: this.age
            })
        })
        
        let info = await res.json();
        this.id = await info.id;
        return await info.id;
    }


}

class VisitDentist extends Visit {
    constructor(date, ...args){
        super(...args);
        this.date = date;
    }

    async createCard(){
        let res = await fetch("https://ajax.test-danit.com/api/v2/cards", {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
            },
        body: JSON.stringify({
            title: 'Визит к стоматологу',
            description: this.description,
            doctor: 'Стоматолог',
            purpose: this.purpose,
            urgency: this.urgency,
            initials: this.initials,
            date: this.date
            })
        })
        let info = await res.json();
        this.id = await info.id;
        return info.id;

    }



}

// By Vlad. Regex form data validation

let reg = /^[A-Z][a-z]*\s[A-Z][a-z]*\s[A-Z][a-z]*$/;
let textReg = /^[\w\s]+(?:,\s*[\w\s]+)*$/;
let presReg = /^(1[0-7]\d|160)\/(5\d|[6-9]\d|90)$/;
let ageReg = /^(?:[2-9]|[1-9]\d)$/;
let bmiReg = /^(1[8-9]|2[0-5])$/;
let dateReg = /^(20[2-9][4-9])-((0[3-9]|1[0-2])-([0-2][0-9]|3[0-1])),([0][8-9]|1[0-9]|2[0-1]):([0-5][0-9])$/;
let counter = 0;


// By Vlad


text.innerHTML = 'No items have been added';

confirmBtn.addEventListener('click', process)

function reset(){
    document.querySelectorAll('input').forEach((item) => item.value = '');
}

let urg = document.querySelector('.urgencyInput');
let doctor = document.querySelector('.doctor');
let fields = document.querySelector('.addFields');
let docTitle = document.querySelector('.doctorTitle');

const main = document.querySelector('.forCards');


document.querySelector('.urgOne').addEventListener('click', function(){
    urg.innerHTML = 'Звичайна';
})
document.querySelector('.urgTwo').addEventListener('click', function(){
    urg.innerHTML = 'Пріоритетна';
})
document.querySelector('.urgThree').addEventListener('click', function(){
    urg.innerHTML = 'Невідкладна';
})


// By Vlad
function sel(param){
    return document.querySelector(param);
}

function setDentist(){
    doctor.innerHTML = 'Стоматолог';
    fields.innerHTML = `<input type="datetime-local" class="dateInput" min="2018-06-07T00:00" max="2025-12-31T00:00" />`;
}
function setCardio(){
    doctor.innerHTML = 'Кардіолог';   
    fields.innerHTML = 
        `<input class="pressureInput" placeholder="Pressure (100-160/50-100)" value="110/70"/>
        <input class="bmiInput" placeholder="Body mass index (18-25)" value="21"/>
        <input class="diseasesInput" placeholder="Diseases" value="No"/>
        <input class="ageCardioInput" placeholder="Age (1-100)" value="33"/>`
}

// By Vlad

function setTherapist(){
    doctor.innerHTML = 'Терапевт';   
    fields.innerHTML = `<input class="ageTherInput" placeholder="Age (1-100)" value="25"/>`;
}


// show function by Nastia

function show(el){
    for (const ch of el.parentElement.children){
        if (ch.classList.contains('cardRoot')){
            ch.classList.toggle('act');
        }
    }
}


// =By Nastia


function editTherapist(el){

    sel('.newInitInput').classList.add('activeInit');
    sel('.newAgeTher').classList.add('activeAge');
    sel('.newPurposeInput').classList.add('activePurp');
    sel('.newDescrInput').classList.add('activeDescr');

    el.addEventListener('click', function f(){

        let newAge = sel('.newAgeTher').value;
        let newPurp = sel('.newPurposeInput').value;
        let newDescr = sel('.newDescrInput').value;
        let newInit = sel('.newInitInput').value;


        sel('.activeInit').outerHTML = newInit;
        sel('.activeAge').outerHTML = newAge;
        sel('.activePurp').outerHTML = newPurp;
        sel('.activeDescr').outerHTML = newDescr;

        let id = 0;
        for (const ch of this.parentElement.children){
            if (ch.classList.contains('id')){
                id = ch.innerHTML;

                fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify({
                        title: 'Визит к терапевту',
                        description: newDescr,
                        doctor: 'Терапевт',
                        purpose: newPurp,
                        initials: newInit,
                        age: newAge
                    })
                }).then(response => response.json())
                    .then(response => console.log(response))

            }
        }
        el.removeEventListener('click', f, true);
    })

}


// By Nastia


function editDentist(el){

    sel('.newInitInput').classList.add('activeInit');
    sel('.newDate').classList.add('activeDate');
    sel('.newPurposeInput').classList.add('activePurp');
    sel('.newDescrInput').classList.add('activeDescr');

    el.addEventListener('click', function f(){

        let newDate = sel('.newDate').value;
        let newPurp = sel('.newPurposeInput').value;
        let newDescr = sel('.newDescrInput').value;
        let newInit = sel('.newInitInput').value;


        sel('.activeInit').outerHTML = newInit;
        sel('.activeDate').outerHTML = newDate;
        sel('.activePurp').outerHTML = newPurp;
        sel('.activeDescr').outerHTML = newDescr;

        let id = 0;
        for (const ch of el.parentElement.children){
            if (ch.classList.contains('id')){
                id = ch.innerHTML;

                fetch("https://ajax.test-danit.com/api/v2/cards", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify({
                        title: 'Визит к стоматологу',
                        description: newDescr,
                        doctor: 'Стоматолог',
                        purpose: newPurp,
                        initials: newInitials,
                        date: newDate,
                    })
                }).then(response => response.json())
                    .then(response => console.log(response))

            }
        }
        el.removeEventListener('click', f, true);
    })

}


// By Nastia


function editCardiologist(el){


    sel('.newInitInput').classList.add('activeInit');
    sel('.newAgeCardioInput').classList.add('activeAge');
    sel('.newPurposeInput').classList.add('activePurp');
    sel('.newDescrInput').classList.add('activeDescr');
    sel('.newDiseasesInput').classList.add('activeDis');
    sel('.newPressureInput').classList.add('activePres');
    sel('.newBmiInput').classList.add('activeBmi');

    el.addEventListener('click', function f(){

        let newInit = sel('.newInitInput').value;
        let newAge = sel('.newAgeCardioInput').value;
        let newPurp = sel('.newPurposeInput').value;
        let newDescr = sel('.newDescrInput').value;
        let newDis = sel('.newDiseasesInput').value;
        let newPres = sel('.newPressureInput').value;
        let newBmi = sel('.newBmiInput').value;

        sel('.activeInit').outerHTML = newInit;
        sel('.activeAge').outerHTML = newAge;
        sel('.activePurp').outerHTML = newPurp;
        sel('.activeDescr').outerHTML = newDescr;
        sel('.activeDis').outerHTML = newDis;
        sel('.activePres').outerHTML = newPres;
        sel('.activeBmi').outerHTML = newBmi;

        let id = 0;
        for (const ch of el.parentElement.children){
            if (ch.classList.contains('id')){
                id = ch.innerHTML;

                fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify({
                        title: 'Візит до кардіолога',
                        description: newDescr,
                        doctor: 'Кардіолог',
                        purpose: newPurp,
                        initials: newInit,
                        pressure: newPres,
                        bmi: newBmi,
                        diseases: newDis,
                        age: newAge
                    })
                }).then(response => response.json())
                    .then(response => console.log(response))

            }
        }

        el.removeEventListener('click', f, true);

    });

}


// By Nastia


function edit(el){

    if (el.parentElement.classList.contains('cardiologist')){

        el.classList.add('cardioEdit')

        for (let init of el.parentElement.children){

            for (let initTwo of init.children){
                initTwo.classList.contains('initialsOutput') ? initTwo.outerHTML = `<input class="newInitInput" type="text" placeholder="Name Surname Patronymic" value="New Random Guy" />` : false;
                for (let initThree of initTwo.children){
                    initThree.classList.contains('descriptionOutput') ? initThree.outerHTML = `<input class="newDescrInput" value="Some new description" />` : false;
                    initThree.classList.contains('purposeOutput') ? initThree.outerHTML = `<input class="newPurposeInput" value="New purpose" />` : false;
                    for (let initFour of initThree.children){
                        initFour.classList.contains('diseasesOutput') ? initFour.outerHTML = `<input class="newDiseasesInput" placeholder="Diseases" value="No"/>` : false;
                        initFour.classList.contains('ageCardioOutput') ? initFour.outerHTML = `<input class="newAgeCardioInput" placeholder="Age (1-100)" value="34"/>` : false;
                        initFour.classList.contains('bmiOutput') ? initFour.outerHTML = `<input class="newBmiInput" placeholder="Body mass index (18-25)" value="22"/>` : false;
                        initFour.classList.contains('pressureOutput') ? initFour.outerHTML = `<input class="newPressureInput" placeholder="Pressure (100-160/50-100)" value="120/80"/>` : false;
                    }
                }
            }
        }

        sel('.cardioEdit').addEventListener('click', editCardiologist(el));

    }
    if (el.parentElement.classList.contains('dentist')){

        el.classList.add('dentistEdit')


        for (let init of el.parentElement.children){

            for (let initTwo of init.children){

                initTwo.classList.contains('initialsOutput') ? initTwo.outerHTML = `<input class="newInitInput" type="text" placeholder="Name Surname Patronymic" value="New Random Guy" />` : false;

                for (let initThree of initTwo.children){
                    initThree.classList.contains('descriptionOutput') ? initThree.outerHTML = `<input class="newDescrInput" value="New description" />` : false;
                    initThree.classList.contains('purposeOutput') ? initThree.outerHTML = `<input class="newPurposeInput" value="New purpose" />` : false;

                    for (let initFour of initThree.children){
                        initFour.classList.contains('dateOutput') ? initFour.outerHTML = `<input class="newDate" type="datetime-local" />` : false;
                    }
                }
            }
        }



        sel('.dentistEdit').addEventListener('click', editDentist(el));

    }
    if (el.parentElement.classList.contains('therapist')){

        el.classList.add('therapistEdit');

        for (let init of el.parentElement.children){

            for (let initTwo of init.children){

                initTwo.classList.contains('initialsOutput') ? initTwo.outerHTML = `<input class="newInitInput" type="text" placeholder="Name Surname Patronymic" value="New Random Guy" />` : false;

                for (let initThree of initTwo.children){
                    initThree.classList.contains('descriptionOutput') ? initThree.outerHTML = `<input class="newDescrInput" value="New description" />` : false;
                    initThree.classList.contains('purposeOutput') ? initThree.outerHTML = `<input class="newPurposeInput" value="New purpose" />` : false;

                    for (let initFour of initThree.children){
                        initFour.classList.contains('ageTherOutput') ? initFour.outerHTML = `<input class="newAgeTher" value="39" />` : false;
                    }
                }
            }
        }

        sel('.therapistEdit').addEventListener('click', editTherapist(el))
    }

}


// By Vlad

let card = `<div class="infoCard">
                <button class="removeBtn" onclick="removeVisit()"> X </button>
                <p class="id"></p>
                <p class="doctorTitle">Doctor: </p>
                <p>Initials: <span class="initialsOutput"></span></p>      
                <div class="cardRoot act">
                    <p>Description: <span class="descriptionOutput"></span></p>
                    <p>Purpose: <span class="purposeOutput"></span></p>
                    <p>Urgency: <span class="urgencyOutput"></span></p>
                    <div class="fieldRoot"></div>
                </div>
                <button class="showButton" onclick="show(this)">Show more</button>
                <button class="editButton" onclick="edit(this)">Edit</button>
            </div>`;

async function createVisit(){
    if (doctor.innerHTML === 'Стоматолог'){addDentistData();}
    if(doctor.innerHTML === 'Кардіолог'){addCardioData();}
    if (doctor.innerHTML === 'Терапевт'){addTherData();}
}


// By Victoria


async function removeVisit(){
    document.querySelectorAll('.removeBtn').forEach((el) => {
        let id = 0;
        for (const ch of el.parentElement.children){
            if (ch.classList.contains('id')){
                id = ch.innerHTML;

                fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
                    method: 'DELETE',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                })
            }

        }
        el.parentElement.remove();

    })
}


// By Victoria

let c = sel('.contentFilter');
let s = sel('.statusFilter');

let react = [c, s];
let created_cards = [];



react.forEach((item) => {


        item.addEventListener('keydown', function(e){

            let lowUrg = sel('.lowCheck').checked;
            let normalUrg = sel('.normalCheck').checked;
            let highUrg = sel('.highCheck').checked;
            let byStatus = sel('.statusFilter').value;
            let byContent = sel('.contentFilter').value;

            console.log(lowUrg)
            console.log(normalUrg)
            console.log(highUrg)
            console.log(byContent)
            console.log(byStatus)
            console.log(created_cards)

            let r = document.querySelector('.filteredCards');

            let ss = new Set(created_cards);

            if (e.keyCode == 8){
                created_cards = [];
                r.innerHTML = "";
            }


            r.innerHTML = Array.from(ss);




            document.querySelectorAll('.infoCard').forEach((card) => {



                let el = `<div class="filteredCard">${card.innerHTML}</div>`;

                // Информация об элементах появляется при вводе текста в input.
                // Галочки пустые, поэтому ничего не отобразится. Нужно снаала поставить галочки.
                // Если есть галочки и вводится текст содержимого - карток появляется внизу.

                let i = 0;

                for (let init of card.children){



                    for (let initTwo of init.children){



                        for (let initThree of initTwo.children){
                            if (initThree.classList.contains('descriptionOutput') || initThree.classList.contains('purposeOutput') ||
                                initThree.classList.contains('initialsOutput')
                            ){
                                if (initThree.classList.contains('urgencyOutput')){
                                    if (lowUrg == true && initThree.textContent === 'Звичайна' ||
                                        normalUrg == true && initThree.textContent === 'Пріоритетна' ||
                                        highUrg == true && initThree.textContent === 'Невідкладна'
                                    ){
                                        created_cards.push(el)
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            }
                            for (let initFour of initThree.children){
                                if (initFour.classList.contains('titleOutput' || initFour.classList.contains('pressureInput') ||
                                    initFour.classList.contains('bmiOutput') || initFour.classList.contains('diseasesOutput')
                                )
                                ){
                                    if (initThree.classList.contains('descriptionOutput') || initThree.classList.contains('purposeOutput') ||
                                        initThree.classList.contains('initialsOutput')
                                    ){

                                        if (initThree.classList.contains('urgencyOutput')){
                                            if (lowUrg == true && initThree.textContent === 'Звичайна' ||
                                                normalUrg == true && initThree.textContent === 'Пріоритетна' ||
                                                highUrg == true && initThree.textContent === 'Невідкладна'
                                            ){
                                                created_cards.push(el)
                                            }
                                            else {
                                                return false;
                                            }
                                        }
                                    }
                                }

                            }

                            if (initThree.classList.contains('urgencyOutput')){
                                if (lowUrg == true && initThree.textContent === 'Звичайна' ||
                                    normalUrg == true && initThree.textContent === 'Пріоритетна' ||
                                    highUrg == true && initThree.textContent === 'Невідкладна'
                                ){
                                    created_cards.push(el);
                                }
                            }
                        }
                    }
                }

            })

        })
    }
)


// By Vlad. Functions for adding data for creating card

async function addTherData(){

    let ageTherVal = sel('.ageTherInput').value;
    let initVal = sel('.initInput').value;
    let urgVal = sel('.urgencyInput').innerHTML
    let purpVal = sel('.purposeInput').value;
    let descrVal = sel('.descrInput').value;

    let addTher =  `<p>Title: <span class="titleOutput">Визит к терапевту</span></p>
                    <p>Age: <span class="ageTherOutput">${ageTherVal}</span></p>`;


    if (ageTherVal !== '' && initVal !== '' && urgVal !== '' && purpVal !== ''
    && descrVal !== '' && reg.test(initVal) && ageReg.test(ageTherVal)){

        main.insertAdjacentHTML('afterbegin', card);  
        sel('.doctorTitle').innerHTML = `<p>Doctor: ${doctor.innerHTML}</p>`
        sel('.initialsOutput').innerHTML = initVal;
        sel('.urgencyOutput').innerHTML = urgVal;
        sel('.purposeOutput').innerHTML = purpVal;
        sel('.descriptionOutput').innerHTML = descrVal;
        sel('.fieldRoot').innerHTML = addTher;
        sel('.infoCard').classList.add('therapist');
       


        let newVisit = new VisitTherapist(ageTherVal, purpVal, descrVal, urgVal, initVal);
        let id = await newVisit.createCard();
        sel('.id').innerHTML = id;

        
        counter++;
        counter > 0 ? text.innerHTML = ' ' : false;
        document.querySelectorAll('input').forEach((item) => item.value = '');

    }
}

// By Vlad

async function addCardioData(){

        let initInput = sel('.initInput').value;
        let urgInput = sel('.urgencyInput').innerHTML;
        let purpVal = sel('.purposeInput').value;
        let descrVal = sel('.descrInput').value;
        let pres = sel('.pressureInput').value;
        let dis = sel('.diseasesInput').value;
        let age = sel('.ageCardioInput').value;
        let bmi = sel('.bmiInput').value;

        let addCardio = `
        <p>Title: <span class="titleOutput">Визит к кардиологу</span></p>
        <p>Pressure: <span class="pressureOutput">${pres}</span></p>
        <p>Diseases: <span class="diseasesOutput">${dis}</span> </p>
        <p>Age: <span class="ageCardioOutput">${age}</span> </p>
        <p>Body mass index: <span class="bmiOutput">${bmi}</span></p>`;


        if (initInput !== '' && urgInput !== '' && purpVal !== '' && descrVal !== '' && pres !== '' && dis !== '' && 
        age !== '' && bmi !== '' && reg.test(initInput) && textReg.test(purpVal) && textReg.test(descrVal) && textReg.test(dis) 
        && presReg.test(pres) && ageReg.test(age) && bmiReg.test(bmi)
        ){

            main.insertAdjacentHTML('afterbegin', card);
            sel('.doctorTitle').innerHTML = `<p>Doctor: ${doctor.innerHTML}</p>`;
            sel('.initialsOutput').innerHTML = initInput;
            sel('.urgencyOutput').innerHTML = urgInput;
            sel('.purposeOutput').innerHTML = purpVal;
            sel('.descriptionOutput').innerHTML = descrVal;
            sel('.fieldRoot').innerHTML = addCardio;
            sel('.infoCard').classList.add('cardiologist');

        let newVisit = new VisitCardiologist(pres, bmi, dis, age, purpVal, descrVal, urgInput, initInput);

        let id = await newVisit.createCard();
        sel('.id').innerHTML = id;
        
        counter++;
        counter > 0 ? text.innerHTML = ' ' : false;
        document.querySelectorAll('input').forEach((item) => item.value = '');

        return id;
        
        }

}

// By Vlad

async function addDentistData(){

    let date = sel('input[type="datetime-local"]').value.split("T");
    let initInput = sel('.initInput').value;
    let urgInput = sel('.urgencyInput').innerHTML;
    let purpVal = sel('.purposeInput').value;
    let descrVal = sel('.descrInput').value;


    let addDen = `<p>Title: <span class="titleOutput">Визит к стоматологу</span></p>
                <p>Date: <span class="dateOutput">${date}</span></p>`

                  if (date !== '' && initInput !== '' && urgInput !== '' && purpVal !== '' && descrVal !== ''
                    && reg.test(initInput) && textReg.test(purpVal) && textReg.test(descrVal) && dateReg.test(date)) {
                    
                        main.insertAdjacentHTML('afterbegin', card);
                    sel('.doctorTitle').innerHTML = `<p>Doctor: ${doctor.innerHTML}</p>`;
                    sel('.initialsOutput').innerHTML = initInput;
                    sel('.urgencyOutput').innerHTML = urgInput;
                    sel('.purposeOutput').innerHTML = purpVal;
                    sel('.descriptionOutput').innerHTML = descrVal;
                    sel('.fieldRoot').innerHTML = addDen;  
                    sel('.infoCard').classList.add('dentist');  
            

                    let newVisit = new VisitDentist(date, purpVal, descrVal, urgInput, initInput);
                    let id = await newVisit.createCard();
                    sel('.id').innerHTML = id;

                    
                    counter++;
                    counter > 0 ? text.innerHTML = ' ' : false;
                    document.querySelectorAll('input').forEach((item) => item.value = '');

                    sel('.closeBtn').addEventListener('click', function(){
                        this.parentElement.remove();
                        newVisit.deleteCard(id);
                    })
                    return id;
                }
}
